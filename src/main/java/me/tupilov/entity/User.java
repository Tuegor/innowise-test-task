package me.tupilov.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class User implements Serializable {
    public static int nextId = 1;

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private List<UserRole> roles;
    private String phoneNumbers;

    public User() {
        id = User.nextId++;
    }

    public User(String firstName, String lastName, String email, List<UserRole> roles, String phoneNumbers) {
        id = User.nextId++;

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.roles = roles;
        this.phoneNumbers = phoneNumbers;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoles() {
        return roles.stream().map(r -> r.toString()).collect(Collectors.joining("+"));
    }

    public void setRoles(String roles) {
        this.roles = Arrays.stream(roles.split("\\+", -1))
                            .map(s -> UserRole.valueOf(s.toUpperCase()))
                            .collect(Collectors.toList());
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
}
