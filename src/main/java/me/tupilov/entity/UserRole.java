package me.tupilov.entity;

public enum UserRole {
    USER,
    CUSTOMER,
    ADMIN,
    PROVIDER,
    SUPER_ADMIN
}
