package me.tupilov;

import me.tupilov.command.CommandExecutor;
import me.tupilov.command.creation.CommandExecutorFactory;
import me.tupilov.command.processing.parsing.ParsingException;
import me.tupilov.command.processing.validation.ValidationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CommandInput {

    private static CommandExecutorFactory commandExecutorFactory = new CommandExecutorFactory();
    private static CommandExecutor commandExecutor;

    public static void main(String[] args) {
        System.out.println("ConsoleCRUD is running. Welcome!");

        InputStreamReader inputStream = new InputStreamReader(System.in);
        BufferedReader bufferedInputStream = new BufferedReader(inputStream);

        while(true) {
            System.out.println("Enter command to execute or q to quit:");

            try {
                String userInput = bufferedInputStream.readLine();
                if(userInput.equals("q"))
                    break;

                String[] commandElements = userInput.split(" -", -1);
                String commandName = commandElements[0];
                List<String> commandArgs = Arrays.stream(commandElements)
                        .skip(1)
                        .collect(Collectors.toCollection(LinkedList::new));

                commandExecutor = commandExecutorFactory.createCommandExecutor(commandName);
                tryExecute(bufferedInputStream, commandArgs);
            }
            catch (IOException e) {
                System.out.println(e.getMessage());
            }
            catch (ParsingException e) {
                if (e.commandNotFound())
                    System.out.println(e.getMessage());
                else {
                    System.out.println(e.getMessage());
                    System.out.println("Command syntax:");
                    System.out.println(commandExecutor.getCommandSyntax());
                }
            }
        }

        System.out.println("Goodbye!");
    }

    private static void tryExecute(BufferedReader bufferedInputStream, List<String> commandArgs) throws IOException, ParsingException {
        String userInput;
        execute:while (true) {
            try {
                execute(commandArgs);
                break execute;
            } catch (ValidationException e) {
                System.out.println(e.getMessage());

                validation:while (true) {
                    System.out.println("Enter the valid argument or a to abort command:");
                    userInput = bufferedInputStream.readLine();
                    if (userInput.equals("a"))
                        break execute;

                    if (userInput.contains(" -"))
                        System.out.println("Space and dash sequence in the argument value is not allowed");
                    else {
                        String arg = commandArgs.get(e.getArgsListIndex());
                        String newArg = arg.split(" ", -1)[0] + " " + userInput;
                        commandArgs.set(e.getArgsListIndex(), newArg);
                        break validation;
                    }
                }
            }
        }
    }

    private static void execute(List<String> argsList) throws ValidationException, ParsingException {
        commandExecutor.execute(argsList);
    }
}
