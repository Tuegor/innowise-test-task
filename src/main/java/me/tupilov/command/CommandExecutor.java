package me.tupilov.command;

import me.tupilov.command.processing.parsing.Parser;
import me.tupilov.command.processing.parsing.ParsingException;
import me.tupilov.command.processing.validation.ValidationException;
import me.tupilov.command.processing.validation.Validator;
import me.tupilov.entity.User;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class CommandExecutor {
    protected static Path dataSource = Paths.get("");

    static {
        try {
            dataSource = Paths.get(CommandExecutor.class
                    .getClassLoader().getResource("users.bin").toURI());
        } catch (URISyntaxException e) {
        }
    }

    protected Parser parser;
    protected Validator validator;

    protected CommandExecutor(Parser parser, Validator validator) {
        this.parser = parser;
        this.validator = validator;
    }

    protected void parse(List<String> argsList) throws ParsingException {
        parser.parse(argsList);
    }

    protected void validate(List<String> argsList) throws ValidationException {
        validator.validate(argsList);
    }

    public void execute(List<String> argsList) throws ParsingException, ValidationException {
        List<User> userList = new LinkedList<>();

        parse(new LinkedList<>(argsList));
        validate(argsList);

        try(InputStream inputStream = Files.newInputStream(dataSource)) {
            if (inputStream.available() > 0) {
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

                User.nextId = objectInputStream.readInt();
                userList = (List<User>) objectInputStream.readObject();

                objectInputStream.close();
                inputStream.close();
            }

            executeHook(argsList, userList);

            try (OutputStream outputStream = Files.newOutputStream(dataSource);
                 ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {

                objectOutputStream.writeInt(User.nextId);
                objectOutputStream.writeObject(userList);
            }
        }
        catch(IOException | ClassNotFoundException e){
            System.out.println("Error while working with file: " + e.getMessage());
            System.out.println("Command aborted");
        }
    }

    protected abstract void executeHook(List<String> argsList, List<User> userList);

    public abstract String getCommandSyntax();
}
