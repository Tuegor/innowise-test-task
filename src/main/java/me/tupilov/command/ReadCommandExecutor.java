package me.tupilov.command;

import me.tupilov.command.processing.parsing.Parser;
import me.tupilov.command.processing.validation.Validator;
import me.tupilov.entity.User;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ReadCommandExecutor extends CommandExecutor {
    private static ReadCommandExecutor object = null;

    private final String commandSyntax =
            "read {-i user_identifier|-a}";

    public ReadCommandExecutor(Parser parser, Validator validator) {
        super(parser, validator);
    }

    @Override
    protected void executeHook(List<String> argsList, List<User> userList) {
        List<User> foundUsers = null;

        String arg = argsList.get(0);
        String[] splitArg = arg.split(" ", -1);
        if (splitArg[0].equals("i")){
            int userId = Integer.parseInt(splitArg[1]);
            foundUsers = userList.stream()
                    .filter(u -> u.getId() == userId)
                    .collect(Collectors.toList());
        }
        else if (splitArg[0].equals("a")){
            foundUsers = userList;
        }

        if (foundUsers.isEmpty())
            System.out.println("No users found");
        else {
            System.out.printf("%12s %-20s %-20s %-25s %-20s %-45s %n",
                    "Identifier", "First name", "Last name", "Email", "User roles", "Phone numbers");
            for (User foundUser : foundUsers)
            {
                System.out.printf("%12d %-20s %-20s %-25s %-20s %-45s %n",
                        foundUser.getId(), foundUser.getFirstName(), foundUser.getLastName(),
                        foundUser.getEmail(), foundUser.getRoles(), foundUser.getPhoneNumbers());
            }
        }
    }

    @Override
    public String getCommandSyntax() {
        return commandSyntax;
    }

    public static ReadCommandExecutor getObject(Parser parser, Validator validator) {
        if (object == null)
            object = new ReadCommandExecutor(parser, validator);
        return object;
    }
}
