package me.tupilov.command.creation;

import me.tupilov.command.*;
import me.tupilov.command.processing.parsing.*;
import me.tupilov.command.processing.validation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CommandExecutorFactory {

    private final Map<String,
                BiFunction<Parser, Validator, CommandExecutor>>
            executorInstantiators;
    private final Map<String, Parser> commandParsers;
    private final Map<String, Validator> commandValidators;

    public CommandExecutorFactory() {
        String[] names =
                {
                        "create",
                        "read",
                        "update",
                        "delete"
                };
        List<BiFunction<Parser, Validator, CommandExecutor>> instantiators =
                Arrays.asList(
                        CreateCommandExecutor::getObject,
                        ReadCommandExecutor::getObject,
                        UpdateCommandExecutor::getObject,
                        DeleteCommandExecutor::getObject
                );
        Parser[] parsers =
                {
                        new MustHaveFirstNameParser(new MustHaveLastNameParser(new MustHaveEmailParser(new MustHaveUserRolesParser(new MustHavePhoneNumbersParser(new EmptyParser()))))),
                        new MustHaveExactOneOfIdOrAllParser(new EmptyParser()),
                        new MustHaveIdParser(new MustHaveAtLeastOneOfFirstNameLastNameEmailRolesPhoneNumbersParser(new EmptyParser())),
                        new MustHaveIdParser(new EmptyParser())
                };
        Validator[] validators =
                {
                        new FirstNameValidator(new LastNameValidator(new EmailValidator(new UserRolesValidator(new PhoneNumbersValidator(null))))),
                        new IdValidator(new AllValidator(null)),
                        new IdValidator(new FirstNameValidator(new LastNameValidator(new EmailValidator(new UserRolesValidator(new PhoneNumbersValidator(null)))))),
                        new IdValidator(null)
                };

        executorInstantiators = IntStream.range(0, names.length).boxed().collect(Collectors.toMap(i -> names[i],
                                                                    i -> instantiators.get(i),
                                                                    (f1, f2) -> f1,
                                                                    HashMap::new));
        commandParsers = IntStream.range(0, names.length).boxed().collect(Collectors.toMap(i -> names[i],
                                                                    i -> parsers[i],
                                                                    (f1, f2) -> f1,
                                                                    HashMap::new));
        commandValidators = IntStream.range(0, names.length).boxed().collect(Collectors.toMap(i -> names[i],
                                                                            i -> validators[i],
                                                                            (f1, f2) -> f1,
                                                                            HashMap::new));
    }

    public CommandExecutor createCommandExecutor(String commandName)
            throws ParsingException{
        if (!executorInstantiators.containsKey(commandName))
            throw new ParsingException(true, "\"" + commandName + "\"");

        return executorInstantiators
                .get(commandName)
                .apply(commandParsers.get(commandName),
                        commandValidators.get(commandName));
    }
}
