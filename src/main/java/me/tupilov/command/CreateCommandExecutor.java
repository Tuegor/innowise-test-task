package me.tupilov.command;

import me.tupilov.command.processing.parsing.Parser;
import me.tupilov.command.processing.validation.Validator;
import me.tupilov.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.IntStream.range;

public class CreateCommandExecutor extends CommandExecutor {
    private static CreateCommandExecutor object = null;

    private final String commandSyntax =
            "create -f First name -l Last name -e email -r role1+role2 -p phone1|phone2";

    protected CreateCommandExecutor(Parser parser, Validator validator) {
        super(parser, validator);
    }

    @Override
    protected void executeHook(List<String> argsList, List<User> userList) {
        User newUser = new User();

        for (String arg : argsList) {
            String[] splitArg = arg.split(" ", -1);
            String argValue = Arrays.stream(splitArg).skip(1).collect(Collectors.joining(" "));

            switch (splitArg[0]) {
                case "f":
                    newUser.setFirstName(argValue);
                    break;
                case "l":
                    newUser.setLastName(argValue);
                    break;
                case "e":
                    newUser.setEmail(argValue);
                    break;
                case "r":
                    newUser.setRoles(argValue);
                    break;
                case "p":
                    newUser.setPhoneNumbers(argValue);
                    break;
            }
        }

        userList.add(newUser);
        System.out.println("New user identifier: " + newUser.getId());
    }

    @Override
    public String getCommandSyntax() {
        return commandSyntax;
    }

    public static CreateCommandExecutor getObject(Parser parser, Validator validator) {
        if (object == null)
            object = new CreateCommandExecutor(parser, validator);
        return object;
    }
}
