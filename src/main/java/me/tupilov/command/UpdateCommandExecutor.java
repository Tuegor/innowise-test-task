package me.tupilov.command;

import me.tupilov.command.processing.parsing.Parser;
import me.tupilov.command.processing.validation.Validator;
import me.tupilov.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UpdateCommandExecutor extends CommandExecutor {
    private static UpdateCommandExecutor object = null;

    private final String commandSyntax =
            "update -i user_identifier {-f First name|-l Last name|" +
            "-e email|-u role1+role2|-p phone1|phone2 ...}";

    public UpdateCommandExecutor(Parser parser, Validator validator) {
        super(parser, validator);
    }

    @Override
    protected void executeHook(List<String> argsList, List<User> userList) {
        User foundUser;

        int userId =
                argsList.stream()
                .map(arg -> arg.split(" ", -1))
                .filter(sarg -> sarg[0].equals("i"))
                .map(sarg -> Integer.parseInt(sarg[1]))
                .findFirst()
                .get();

        Optional<User> foundUserOpt = userList.stream().filter(u -> u.getId() == userId).findFirst();
        if (!foundUserOpt.isPresent())
            System.out.println("User not found");
        else {
            foundUser = foundUserOpt.get();
            for (String arg : argsList)
            {
                String[] splitArg = arg.split(" ", -1);
                String argName = splitArg[0];
                String argValue = Arrays.stream(splitArg).skip(1).collect(Collectors.joining(" "));

                switch(argName) {
                    case "f":
                        foundUser.setFirstName(argValue);
                        break;
                    case "l":
                        foundUser.setLastName(argValue);
                        break;
                    case "e":
                        foundUser.setEmail(argValue);
                        break;
                    case "r":
                        foundUser.setRoles(argValue);
                        break;
                    case "p":
                        foundUser.setPhoneNumbers(argValue);
                        break;
                }
            }
        }
    }

    @Override
    public String getCommandSyntax() {
        return commandSyntax;
    }

    public static UpdateCommandExecutor getObject(Parser parser, Validator validator) {
        if (object == null)
            object = new UpdateCommandExecutor(parser, validator);
        return object;
    }
}
