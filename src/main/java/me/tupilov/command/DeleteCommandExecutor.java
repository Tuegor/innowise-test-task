package me.tupilov.command;

import me.tupilov.command.processing.parsing.Parser;
import me.tupilov.command.processing.validation.Validator;
import me.tupilov.entity.User;

import java.util.List;
import java.util.ListIterator;

public class DeleteCommandExecutor extends CommandExecutor {
    private static DeleteCommandExecutor object = null;

    private final String commandSyntax =
            "delete -i user_identifier";

    public DeleteCommandExecutor(Parser parser, Validator validator) {
        super(parser, validator);
    }

    @Override
    protected void executeHook(List<String> argsList, List<User> userList) {
        int userId =
                argsList.stream()
                        .map(arg -> arg.split(" ", -1))
                        .filter(sarg -> sarg[0].equals("i"))
                        .map(sarg -> Integer.parseInt(sarg[1]))
                        .findFirst()
                        .get();

        ListIterator<User> iterator = userList.listIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == userId) {
                iterator.remove();
                break;
            }
        }
    }

    @Override
    public String getCommandSyntax() {
        return commandSyntax;
    }

    public static DeleteCommandExecutor getObject(Parser parser, Validator validator) {
        if (object == null)
            object = new DeleteCommandExecutor(parser, validator);
        return object;
    }
}
