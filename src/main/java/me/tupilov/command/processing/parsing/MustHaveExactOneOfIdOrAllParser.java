package me.tupilov.command.processing.parsing;

import java.util.List;
import java.util.ListIterator;

public class MustHaveExactOneOfIdOrAllParser extends Parser {
    public MustHaveExactOneOfIdOrAllParser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public void parse(List<String> argsList) throws ParsingException {
        ListIterator<String> iterator = argsList.listIterator();
        boolean hasId = false;
        boolean hasAll = false;

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("i\\b.*")) {
                if (hasId)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasId = true;
                iterator.remove();
            }
            if (next.matches("a\\b.*")){
                if (hasAll)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasAll = true;
                iterator.remove();
            }
            if (hasId && hasAll)
                throw new ParsingException(false, "Invalid combination of arguments: i and a");
        }
        if (!hasId && !hasAll)
            throw new ParsingException(false, "Missing argument: i or a");

        if (nextParser != null)
            nextParser.parse(argsList);
    }
}
