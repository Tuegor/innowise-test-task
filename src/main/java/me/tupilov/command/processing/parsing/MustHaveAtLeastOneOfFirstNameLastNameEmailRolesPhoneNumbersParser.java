package me.tupilov.command.processing.parsing;

import java.util.List;
import java.util.ListIterator;

public class MustHaveAtLeastOneOfFirstNameLastNameEmailRolesPhoneNumbersParser extends Parser {
    public MustHaveAtLeastOneOfFirstNameLastNameEmailRolesPhoneNumbersParser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public void parse(List<String> argsList) throws ParsingException {
        ListIterator<String> iterator = argsList.listIterator();
        boolean hasFirst = false;
        boolean hasLast = false;
        boolean hasEmail = false;
        boolean hasRoles = false;
        boolean hasPhones = false;

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("f\\b.*")) {
                if (hasFirst)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasFirst = true;
                iterator.remove();
            }
            if (next.matches("l\\b.*")) {
                if (hasLast)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasLast = true;
                iterator.remove();
            }
            if (next.matches("e\\b.*")) {
                if (hasEmail)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasEmail = true;
                iterator.remove();
            }
            if (next.matches("r\\b.*")) {
                if (hasRoles)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasRoles = true;
                iterator.remove();
            }
            if (next.matches("p\\b.*")) {
                if (hasPhones)
                    throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
                hasPhones = true;
                iterator.remove();
            }
        }
        if (!hasFirst && !hasLast && !hasEmail && !hasRoles && !hasPhones)
            throw new ParsingException(false, "Missing argument: f or l or e or r or p");

        if (nextParser != null)
            nextParser.parse(argsList);
    }
}
