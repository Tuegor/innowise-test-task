package me.tupilov.command.processing.parsing;

import java.util.List;
import java.util.ListIterator;

public class MustHaveFirstNameParser extends Parser {
    public MustHaveFirstNameParser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public void parse(List<String> argsList) throws ParsingException {
        boolean has = false;
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            if (iterator.next().matches("f\\b.*")) {
                iterator.remove();
                has = true;
                break;
            }
        }
        if (!has)
            throw new ParsingException(false, "Missing argument: f");

        if (nextParser != null)
            nextParser.parse(argsList);
    }
}
