package me.tupilov.command.processing.parsing;

import java.util.List;
import java.util.ListIterator;

public class MustHaveLastNameParser extends Parser {
    public MustHaveLastNameParser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public void parse(List<String> argsList) throws ParsingException {
        boolean has = false;
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            if (iterator.next().matches("l\\b.*")) {
                iterator.remove();
                has = true;
                break;
            }
        }
        if (!has)
            throw new ParsingException(false, "Missing argument: l");

        if (nextParser != null)
            nextParser.parse(argsList);
    }
}
