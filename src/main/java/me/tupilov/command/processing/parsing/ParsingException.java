package me.tupilov.command.processing.parsing;

public class ParsingException extends Exception {
    private boolean commandNotFound;

    public ParsingException(boolean commandNotFound, String message) {
        super(message);
        this.commandNotFound = commandNotFound;
    }

    public ParsingException(boolean commandNotFound, String message, Throwable cause)
    {
        super(message, cause);
        this.commandNotFound = commandNotFound;
    }

    public boolean commandNotFound() {
        return commandNotFound;
    }

    @Override
    public String getMessage() {
        if (commandNotFound)
            return "Command not found: " + super.getMessage();
        else
            return "Wrong command arguments: " + super.getMessage();
    }
}
