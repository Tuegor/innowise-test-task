package me.tupilov.command.processing.parsing;

import java.util.List;

public abstract class Parser {
    protected Parser nextParser = null;

    abstract public void parse(List<String> argsList) throws ParsingException;
}
