package me.tupilov.command.processing.parsing;

import java.util.List;

public class EmptyParser extends Parser {
    @Override
    public void parse(List<String> argsList) throws ParsingException {
        if (!argsList.isEmpty())
            throw new ParsingException(false, "Unrecognized arguments: " + "\"" + "-" + String.join(" -", argsList) + "\"");
    }
}
