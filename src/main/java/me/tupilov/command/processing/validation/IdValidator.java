package me.tupilov.command.processing.validation;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class IdValidator extends Validator {
    public IdValidator(Validator nextValidator) {
        this.nextValidator = nextValidator;
    }

    @Override
    public void validate(List<String> argsList) throws ValidationException {
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("i\\b.*")) {
                String argValue = Arrays
                        .stream(next.split(" ", -1))
                        .skip(1)
                        .collect(Collectors.joining(" "));

                try {
                    Integer.parseInt(argValue);
                } catch (NumberFormatException e) {
                    throw new ValidationException(iterator.previousIndex(), "Id value is not a number of type integer: " + "\"" + argValue + "\"", e);
                }
            }
        }

        if (nextValidator != null)
            nextValidator.validate(argsList);
    }
}
