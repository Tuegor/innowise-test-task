package me.tupilov.command.processing.validation;

public class ValidationException extends Exception {
    private int argsListIndex;

    public ValidationException(int argsListIndex, String message) {
        super(message);
        this.argsListIndex = argsListIndex;
    }

    public ValidationException(int argsListIndex, String message, Throwable cause) {
        super(message, cause);
        this.argsListIndex = argsListIndex;
    }

    public int getArgsListIndex() {
        return argsListIndex;
    }
}
