package me.tupilov.command.processing.validation;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class LastNameValidator extends Validator {
    public LastNameValidator(Validator nextValidator) {
        this.nextValidator = nextValidator;
    }

    @Override
    public void validate(List<String> argsList) throws ValidationException {
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("l\\b.*")) {
                String argValue = Arrays
                        .stream(next.split(" ", -1))
                        .skip(1)
                        .collect(Collectors.joining(" "));
                boolean valid = argValue.matches("[A-Z][a-z]*");
                if (!valid)
                    throw new ValidationException(iterator.previousIndex(), "Second name value not valid: " + "\"" + argValue + "\"");
            }
        }

        if (nextValidator != null)
            nextValidator.validate(argsList);
    }
}
