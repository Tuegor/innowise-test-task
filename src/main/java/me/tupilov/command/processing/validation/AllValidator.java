package me.tupilov.command.processing.validation;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class AllValidator extends Validator {
    public AllValidator(Validator nextValidator) {
        this.nextValidator = nextValidator;
    }

    @Override
    public void validate(List<String> argsList) throws ValidationException {
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("a\\b.*")) {
                boolean valid = next.equals("a");
                if (!valid) {
                    System.out.println("Argument a does not allow a value: " + "\"" + next.substring(2) + "\"");
                    System.out.println("Dropping value of a");
                    iterator.set("a");
                }
            }
        }

        if (nextValidator != null)
            nextValidator.validate(argsList);
    }
}
