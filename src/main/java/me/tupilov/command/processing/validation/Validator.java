package me.tupilov.command.processing.validation;

import java.util.List;

public abstract class Validator {
    protected Validator nextValidator = null;

    abstract public void validate(List<String> argsList) throws ValidationException;
}
