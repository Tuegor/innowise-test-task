package me.tupilov.command.processing.validation;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UserRolesValidator extends Validator {
    public UserRolesValidator(Validator nextValidator) {
        this.nextValidator = nextValidator;
    }

    @Override
    public void validate(List<String> argsList) throws ValidationException {
        ListIterator<String> iterator = argsList.listIterator();

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.matches("r\\b.*")) {
                String argValue = Arrays
                        .stream(next.split(" ", -1))
                        .skip(1)
                        .collect(Collectors.joining(" "));
                boolean valid = argValue.matches("(?i)((user)|(customer))\\+((admin)|(provider))")
                                || argValue.matches("(?i)((admin)|(provider))\\+((user)|(customer))")
                                || argValue.matches("(?i)(admin)|(provider)|(user)|(customer)")
                                || argValue.matches("(?i)super_admin");
                if (!valid)
                    throw new ValidationException(iterator.previousIndex(), "User roles value not valid: " + "\"" +argValue + "\"");
            }
        }

        if (nextValidator != null)
            nextValidator.validate(argsList);
    }
}
